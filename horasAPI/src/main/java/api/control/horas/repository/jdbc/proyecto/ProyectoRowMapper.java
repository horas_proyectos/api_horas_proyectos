package api.control.horas.repository.jdbc.proyecto;

import api.control.horas.model.Proyecto;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ProyectoRowMapper implements RowMapper<Proyecto> {

    @Override
    public Proyecto mapRow(ResultSet rs, int rowNum) throws SQLException {

        Proyecto proyecto = new Proyecto();

        proyecto.setProyectoid(rs.getInt("proyectoid"));
        proyecto.setNombre(rs.getString("nombre"));
        proyecto.setDescripcion(rs.getString("descripcion"));

        //empleado.setParticipanList(null);

        return proyecto;
    }
}
