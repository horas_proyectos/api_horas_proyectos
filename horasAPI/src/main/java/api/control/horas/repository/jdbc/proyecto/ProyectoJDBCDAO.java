package api.control.horas.repository.jdbc.proyecto;

import api.control.horas.model.Proyecto;
import api.control.horas.utilities.Filtro;
import api.control.horas.utilities.TipoFiltro;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Repository
public class ProyectoJDBCDAO {

    @Autowired
    JdbcTemplate jdbcTemplate;

    public static final String SELECT_COUNT = "SELECT COUNT(*) FROM proyectos p";
    public static final String SELECT_ALL = "SELECT * FROM proyectos p";
    public static final String SELECT_BY_ID = "SELECT * FROM proyectos WHERE proyectoid = ?";

    private static final String INSERT =  "INSERT INTO proyectos(nombre, descripcion) VALUES(?, ?)";
    private static final String UPDATE =  "UPDATE proyectos SET nombre=?, descripcion=? WHERE proyectoid=?";
    private static final String DELETE =  "DELETE FROM proyectos WHERE proyectoid=?";

    private static final String ORDER_BY = " ORDER BY p.proyectoid ASC";

    private static final String SUM_TOTAL_HORAS_PROYECTO_BY_ID = "SELECT SUM(p.horas) FROM participan as p " +
                                                                 "INNER JOIN proyectos as pr " +
                                                                 "ON (p.proyectoid = pr.proyectoid) " +
                                                                 "WHERE p.proyectoid =?";


    public int count() {
        return jdbcTemplate.queryForObject(SELECT_COUNT, Integer.class);
    }

    public Proyecto findById (Integer id){

        Proyecto proyecto = this.jdbcTemplate.query(new PreparedStatementCreator() {

            @Override
            public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {

                PreparedStatement ps = connection.prepareStatement(SELECT_BY_ID, Statement.CLOSE_CURRENT_RESULT);
                ps.setInt(1, id);
                return ps;
            }
        }, new ProyectoResultSetExtractor());
        return proyecto;
    }

    public List<Proyecto> findAll(String search) {

        List<Filtro> filtros = this.generateQueryParams(search);

        List<Proyecto> proyectos = this.jdbcTemplate.query(
                new PreparedStatementCreator() {
                    @Override
                    public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {

                        String query = SELECT_ALL;

                        for (Filtro filtro : filtros) {
                            query += " WHERE " + filtro.getCondicion() + " ";
                        }

                        query += ORDER_BY;

                        PreparedStatement ps = connection.prepareStatement(query,
                                Statement.CLOSE_CURRENT_RESULT);

                        assignQueryParams(ps, 1, filtros);
                        return ps;
                    }

                },
                new ProyectoRowMapper());

        return proyectos;
    }

    private List<Filtro> generateQueryParams(String searchString) {

        List<Filtro> filtros = new ArrayList<>();
        Pattern pattern = Pattern.compile("([\\w\\d]+)\\.([\\w\\d]+)(:|<|<=|>=|>)([\\w\\d]+)");
        Matcher matcher = pattern.matcher(searchString);

        // Para cada condición
        while (matcher.find()) {

            // Extraemos entidad
            String entidad = matcher.group(1);
            String atributo = matcher.group(2);
            String condicion = matcher.group(3);
            String valor = matcher.group(4);

            Filtro filtro = this.buildCondicion(entidad, atributo, condicion, valor);
            if (filtro != null) {
                filtros.add(filtro);
            }
        }

        return filtros;
    }

    private Filtro buildCondicion(String entidad, String atributo,
                                  String condicion, String valor) {

        switch (entidad) {
            case "proyecto":

                switch (atributo) {

                    case "nombre":
                        if (condicion.equals(":")) {
                            condicion = "like";
                            valor = "%" + valor + "%";
                        }
                        return new Filtro("p.nombre " + condicion + " ? ",
                                TipoFiltro.STRING, valor);

                    case "descripcion":
                        if (condicion.equals(":")) {
                            condicion = "like";
                            valor = "%" + valor + "%";
                        }
                        return new Filtro("p.descripcion " + condicion + " ? ",
                                TipoFiltro.STRING, valor);

                    default:
                        throw new IllegalArgumentException("Condición no valida.");
                }
        }
        return new Filtro("p.horas " + condicion + " ? ",
                TipoFiltro.FLOAT, Float.parseFloat(valor));
    }

    private void assignQueryParams(PreparedStatement ps, int initPosition, List<Filtro> filtros) throws SQLException {

        int i = initPosition;
        for(Filtro filtro: filtros) {

            switch (filtro.getTipoFiltro()) {

                case STRING:
                    ps.setString(i, (String)filtro.getValor());
                    break;

                case INTEGER:
                    ps.setInt(i, (Integer)filtro.getValor());
                    break;

                case FLOAT:
                    ps.setFloat(i, (Float)filtro.getValor());
                    break;

                default:
                    System.out.println(filtro.getTipoFiltro());
                    throw new IllegalArgumentException("Condición no valida.");
            }

            i++;
        }
    }

    public Proyecto insert(Proyecto proyecto) {

        KeyHolder keyHolder = new GeneratedKeyHolder();



        int resultadoOperacion = this.jdbcTemplate.update(
                new PreparedStatementCreator() {
                    @Override
                    public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {

                        PreparedStatement ps =   connection.prepareStatement(INSERT, Statement.CLOSE_CURRENT_RESULT);
                        ps.setString(1, proyecto.getNombre());
                        ps.setString(2, proyecto.getDescripcion());
                        return ps;
                    }
                }, keyHolder);

        if(resultadoOperacion != 1) {
            return null;
        }

        Integer newId;
        //Sistema para coger siempre el id del keyHolder y que no de errores
        if (keyHolder.getKeys().size() > 1) {
            newId = (Integer) keyHolder.getKeys().get("empleadoid");
        } else {
            newId= keyHolder.getKey().intValue();
        }

        proyecto.setProyectoid(newId);

        return proyecto;
    }

    public Boolean update(Proyecto proyecto) {

        int resultadoOperacion = this.jdbcTemplate.update(
                new PreparedStatementCreator() {
                    @Override
                    public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {

                        PreparedStatement ps =   connection.prepareStatement(UPDATE, Statement.CLOSE_CURRENT_RESULT);
                        ps.setString(1, proyecto.getNombre());
                        ps.setString(2, proyecto.getDescripcion());
                        ps.setInt(3, proyecto.getProyectoid());
                        return ps;
                    }
                });

        return resultadoOperacion == 1;
    }

    public Boolean delete(Proyecto proyecto) {

        int resultadoOperacion = this.jdbcTemplate.update(
                new PreparedStatementCreator() {
                    @Override
                    public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {

                        PreparedStatement ps =   connection.prepareStatement(DELETE, Statement.CLOSE_CURRENT_RESULT);
                        ps.setInt(1, proyecto.getProyectoid());
                        return ps;
                    }
                });

        return resultadoOperacion == 1;
    }

    public Double sumHorasProyecto(Integer id) {

        Double sumaTotalHorasProyecto =  jdbcTemplate.query(new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {

                PreparedStatement ps;

                ps = connection.prepareStatement(SUM_TOTAL_HORAS_PROYECTO_BY_ID, Statement.CLOSE_CURRENT_RESULT);

                ps.setInt(1, id);
                return ps;
            }
        }, new ResultSetExtractor<Double>() {
            @Override
            public Double extractData(ResultSet rs) throws SQLException, DataAccessException {

                if(!rs.next()){
                    return null;
                }

                return rs.getDouble(1);
            }
        });

        return sumaTotalHorasProyecto;
    }
}
