package api.control.horas.repository.jdbc.participan;

import api.control.horas.model.Participan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.List;

@Repository
public class ParticipanJDBCDAO {

    @Autowired
    JdbcTemplate jdbcTemplate;

    private static final String SELECT_COUNT = "SELECT COUNT(*) FROM participan";
    private static final String SELECT_ALL = "SELECT * FROM participan ORDER BY registroid ASC ";
    private static final String SELECT_ALL_PARTICIPAN_WITH_EMPLEADO =  "SELECT * FROM participan as p "
                                                                     + "INNER JOIN empleados as e "
                                                                     + "ON (e.empleadoid = p.empleadoid) "
                                                                     + "ORDER BY registroid ASC";
    private static final String SELECT_ALL_PARTICIPAN_WITH_PROYECTO =  "SELECT * FROM participan as pa "
                                                                    + "INNER JOIN proyectos as p "
                                                                    + "ON (p.proyectoid = pa.proyectoid) "
                                                                    + "ORDER BY registroid ASC";
    private static final String SELECT_ALL_PARTICIPAN_WITH_ALL =  "SELECT * FROM participan as pa "
                                                                    + "INNER JOIN proyectos as p "
                                                                    + "ON (p.proyectoid = pa.proyectoid) "
                                                                    + "INNER JOIN empleados as e "
                                                                    + "ON (e.empleadoid = pa.empleadoid) "
                                                                    + "ORDER BY registroid ASC";
    private static final String SELECT_BY_ID = "SELECT * FROM participan WHERE registroid = ?";
    private static final String SELECT_BY_ID_PARTICIPAN_WITH_EMPLEADO =  "SELECT * FROM participan as p "
                                                                        + "INNER JOIN empleados as e "
                                                                        + "ON (e.empleadoid = p.empleadoid) "
                                                                        + "WHERE p.registroid = ?";
    private static final String SELECT_BY_ID_PARTICIPAN_WITH_PROYECTO =  "SELECT * FROM participan as p "
                                                                    + "INNER JOIN proyectos as pr "
                                                                    + "ON (pr.proyectoid = p.proyectoid) "
                                                                    + "WHERE p.registroid = ?";
    private static final String SELECT_BY_ID_PARTICIPAN_WITH_ALL =  "SELECT * FROM participan as p "
                                                                    + "INNER JOIN proyectos as pr "
                                                                    + "ON (pr.proyectoid = p.proyectoid) "
                                                                    + "INNER JOIN empleados as e "
                                                                    + "ON (e.empleadoid = p.empleadoid) "
                                                                    + "WHERE p.registroid = ?";
    private static final String INSERT =  "INSERT INTO participan(empleadoid, proyectoid, horas) VALUES(?,?,?)";
    private static final String UPDATE =  "UPDATE participan SET horas=?, empleadoid=?, proyectoid=? WHERE registroid=?";
    private static final String DELETE =  "DELETE FROM participan WHERE registroid=?";

    private static final String SUM_TOTAL_HORAS_PARTICIPAN_BY_ID = "SELECT SUM(p.horas) FROM participan as p " +
                                                                 "INNER JOIN empleados as e " +
                                                                 "ON (p.empleadoid = e.empleadoid) " +
                                                                 "INNER JOIN proyectos as pr " +
                                                                 "ON (p.proyectoid = pr.proyectoid) " +
                                                                 "WHERE p.empleadoid =? " +
                                                                 "AND p.proyectoid =?";


    public int count() {
        return jdbcTemplate.queryForObject(ParticipanJDBCDAO.SELECT_COUNT, Integer.class);
    }

    public List<Participan> findAll(Boolean withEmpleado, Boolean withProyecto, Boolean withAll) {

        List<Participan> participanList = this.jdbcTemplate.query(
                new PreparedStatementCreator() {
                    @Override
                    public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {

                        if (withAll){
                            return connection.prepareStatement(SELECT_ALL_PARTICIPAN_WITH_ALL, Statement.CLOSE_CURRENT_RESULT);
                        }else if (withProyecto){
                            return connection.prepareStatement(SELECT_ALL_PARTICIPAN_WITH_PROYECTO, Statement.CLOSE_CURRENT_RESULT);
                        }else if (withEmpleado) {
                            return connection.prepareStatement(SELECT_ALL_PARTICIPAN_WITH_EMPLEADO, Statement.CLOSE_CURRENT_RESULT);
                        }else
                            return connection.prepareStatement(SELECT_ALL, Statement.CLOSE_CURRENT_RESULT);
                    }
                }
                , new ParticipanRowMapper(withEmpleado, withProyecto, withAll));

        return participanList;
    }

    public Participan findById(Integer id, Boolean withEmpleado, Boolean withProyecto, Boolean withAll) {

        Participan participan = this.jdbcTemplate.query(new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {

                PreparedStatement ps;

                if (withAll) {
                    ps = connection.prepareStatement(SELECT_BY_ID_PARTICIPAN_WITH_ALL,
                            Statement.CLOSE_CURRENT_RESULT);
                }else if (withProyecto) {
                    ps = connection.prepareStatement(SELECT_BY_ID_PARTICIPAN_WITH_PROYECTO,
                            Statement.CLOSE_CURRENT_RESULT);
                }else if (withEmpleado) {
                    ps = connection.prepareStatement(SELECT_BY_ID_PARTICIPAN_WITH_EMPLEADO,
                            Statement.CLOSE_CURRENT_RESULT);
                } else {
                    ps = connection.prepareStatement(SELECT_BY_ID, Statement.CLOSE_CURRENT_RESULT);
                }

                ps.setInt(1, id);
                return ps;
            }
        }, new ParticipanResultSetExtractor(withEmpleado, withAll, withProyecto));

        return participan;
    }

    public Participan insert(Participan participan) {

        KeyHolder keyHolder = new GeneratedKeyHolder();

        int resultadoOperacion = this.jdbcTemplate.update(
                new PreparedStatementCreator() {
                    @Override
                    public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {

                        PreparedStatement ps = connection.prepareStatement(INSERT, Statement.CLOSE_CURRENT_RESULT);
                        ps.setInt(1, participan.getEmpleadoid());
                        ps.setInt(2, participan.getProyectoid());
                        ps.setFloat(3, participan.getHoras());
                        return ps;
                    }
                }, keyHolder);

        if (resultadoOperacion != 1) {
            return null;
        }

        Integer newId;
        //Sistema para coger siempre el id del keyHolder y que no de errores
        if (keyHolder.getKeys().size() > 1) {
            newId = (Integer) keyHolder.getKeys().get("registroid");
        } else {
            newId= keyHolder.getKey().intValue();
        }
        participan.setRegistroid(newId);

        return participan;
    }

    public Boolean update(Participan participan){

        int resultadoOperacion = this.jdbcTemplate.update(
                new PreparedStatementCreator() {
                    @Override
                    public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {

                        PreparedStatement ps =   connection.prepareStatement(UPDATE, Statement.CLOSE_CURRENT_RESULT);
                        //No pongo ni empleadoid ni proyectoid porque son claves ajenas y dan error si intentas modificarlas
                        ps.setFloat(1, participan.getHoras());
                        ps.setInt(2, participan.getEmpleadoid());
                        ps.setInt(3, participan.getProyectoid());
                        ps.setInt(4, participan.getRegistroid());
                        return ps;
                    }
                });

        return resultadoOperacion == 1;
    }

    public Boolean delete(Participan participan){
        int resultadoOperacion = this.jdbcTemplate.update(
                new PreparedStatementCreator() {
                    @Override
                    public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {

                        PreparedStatement ps =   connection.prepareStatement(DELETE, Statement.CLOSE_CURRENT_RESULT);
                        ps.setInt(1, participan.getRegistroid());
                        return ps;
                    }
                });

        return resultadoOperacion == 1;
    }

    public Double sumHorasParticipan(Integer idEmpleado, Integer idProyecto) {

        Double sumaTotalHorasParticipan =  jdbcTemplate.query(new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {

                PreparedStatement ps;

                ps = connection.prepareStatement(SUM_TOTAL_HORAS_PARTICIPAN_BY_ID, Statement.CLOSE_CURRENT_RESULT);

                ps.setInt(1, idEmpleado);
                ps.setInt(2, idProyecto);
                return ps;
            }
        }, new ResultSetExtractor<Double>() {
            @Override
            public Double extractData(ResultSet rs) throws SQLException, DataAccessException {

                if(!rs.next()){
                    return null;
                }

                return rs.getDouble(1);
            }
        });

        return sumaTotalHorasParticipan;
    }
}
