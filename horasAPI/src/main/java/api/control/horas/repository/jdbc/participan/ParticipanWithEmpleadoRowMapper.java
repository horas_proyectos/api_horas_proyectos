package api.control.horas.repository.jdbc.participan;

import api.control.horas.model.Empleado;
import api.control.horas.model.Participan;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ParticipanWithEmpleadoRowMapper implements RowMapper<Participan> {
    @Override
    public Participan mapRow(ResultSet rs, int rowNum) throws SQLException {

        Participan participan = new Participan();
        participan.setRegistroid(rs.getInt("registroid"));
        participan.setEmpleadoid(rs.getInt("empleadoid"));
        participan.setProyectoid(rs.getInt("proyectoid"));
        participan.setHoras(rs.getFloat("horas"));

        Empleado empleado = new Empleado();

        empleado.setNombreempleado(rs.getString("nombreempleado"));
        empleado.setTelefono(rs.getString("telefono"));
        participan.setEmpleado(empleado);

        return participan;
    }
}
