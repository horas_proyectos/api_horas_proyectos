package api.control.horas.repository.jdbc.proyecto;

import api.control.horas.model.Proyecto;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ProyectoResultSetExtractor implements ResultSetExtractor<Proyecto> {
        @Override
        public Proyecto extractData(ResultSet rs) throws SQLException, DataAccessException {

            if(!rs.next()){
                return null;
            }

            Proyecto proyecto = new Proyecto();

            proyecto.setProyectoid(rs.getInt("proyectoid"));
            proyecto.setNombre(rs.getString("nombre"));
            proyecto.setDescripcion(rs.getString("descripcion"));

            return proyecto;
        }
}
