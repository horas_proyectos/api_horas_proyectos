package api.control.horas.repository.jdbc.empleado;

import api.control.horas.model.Empleado;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import java.sql.ResultSet;
import java.sql.SQLException;

/**Esta clase obtiene un solo resultado, le añade los valores a un objeto y lo devuelve*/
public class EmpleadoResultSetExtractor implements ResultSetExtractor<Empleado> {
    @Override
    public Empleado extractData(ResultSet rs) throws SQLException, DataAccessException {

        if(!rs.next()){
            return null;
        }

        Empleado empleado = new Empleado();

        empleado.setEmpleadoid(rs.getInt("empleadoid"));
        empleado.setNombreempleado(rs.getString("nombreempleado"));
        empleado.setTelefono(rs.getString("telefono"));
        return empleado;
    }
}