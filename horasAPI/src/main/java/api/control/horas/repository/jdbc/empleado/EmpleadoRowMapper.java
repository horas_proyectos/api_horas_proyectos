package api.control.horas.repository.jdbc.empleado;

import api.control.horas.model.Empleado;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**Esta clase se encarga de mapear cada fila del resultado de la query y devuelve muchos objetos de uno en uno*/
public class EmpleadoRowMapper implements RowMapper<Empleado> {

    @Override
    public Empleado mapRow(ResultSet rs, int rowNum) throws SQLException {

        Empleado empleado = new Empleado();

        empleado.setEmpleadoid(rs.getInt("empleadoid"));
        empleado.setNombreempleado(rs.getString("nombreempleado"));
        empleado.setTelefono(rs.getString("telefono"));

        //empleado.setParticipanList(null);

        return empleado;
    }
}
