package api.control.horas.repository.jdbc.participan;

import api.control.horas.model.Empleado;
import api.control.horas.model.Participan;
import api.control.horas.model.Proyecto;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ParticipanResultSetExtractor implements ResultSetExtractor<Participan> {

    private Boolean withEmpleado;
    private Boolean withProyecto;
    private Boolean withAll;

    @Override
    public Participan extractData(ResultSet rs) throws SQLException, DataAccessException {

        if(!rs.next()){
            return null;
        }
        Participan participan = new Participan();

        participan.setRegistroid(rs.getInt("registroid"));
        participan.setEmpleadoid(rs.getInt("empleadoid"));
        participan.setProyectoid(rs.getInt("proyectoid"));
        participan.setHoras(rs.getFloat("horas"));

        Empleado empleado = null;
        Proyecto proyecto = null;

        if (withAll) {
            empleado = new Empleado();
            empleado.setEmpleadoid(rs.getInt("empleadoid"));
            //TODO Al tener junto todo en el resultSet y 2 valores con el mismo nombre, solo coge 1 de los 2
            // Solución: Cambiarle el nombre a uno de ellos en la BD
            empleado.setNombreempleado(rs.getString("nombreempleado"));
            empleado.setTelefono(rs.getString("telefono"));
            
            proyecto = new Proyecto();
            proyecto.setProyectoid(rs.getInt("proyectoid"));
            proyecto.setNombre(rs.getString("nombre"));
            proyecto.setDescripcion(rs.getString("descripcion"));
        }else if (withProyecto) {
            proyecto = new Proyecto();
            proyecto.setProyectoid(rs.getInt("proyectoid"));
            proyecto.setNombre(rs.getString("nombre"));
            proyecto.setDescripcion(rs.getString("descripcion"));
        }else if (withEmpleado) {
            empleado = new Empleado();
            empleado.setEmpleadoid(rs.getInt("empleadoid"));
            empleado.setNombreempleado(rs.getString("nombreempleado"));
            empleado.setTelefono(rs.getString("telefono"));
        }

        participan.setEmpleado(empleado);
        participan.setProyecto(proyecto);

        return participan;
    }
    //Constructor de esta clase con el atributo withEmpleado
    public ParticipanResultSetExtractor(Boolean withEmpleado, Boolean withAll, Boolean withProyecto) {
        this.withEmpleado = withEmpleado;
        this.withAll = withAll;
        this.withProyecto = withProyecto;
    }
}
