package api.control.horas.repository.jdbc.empleado;

import api.control.horas.model.Empleado;
import api.control.horas.utilities.Filtro;
import api.control.horas.utilities.TipoFiltro;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Repository
/**Esta clase es un repositorio en el que se crean las querys y las operaciones CRUD*/
public class EmpleadoJDBCDAO {

    @Autowired
    JdbcTemplate jdbcTemplate;

    public static final String SELECT_COUNT = "SELECT COUNT(*) FROM EMPLEADOS e";

    public static final String SELECT_ALL = "SELECT * FROM EMPLEADOS e";
    public static final String SELECT_BY_ID = "SELECT * FROM EMPLEADOS WHERE empleadoid = ?";

    private static final String INSERT =  "INSERT INTO empleados(nombreempleado, telefono) VALUES(?, ?)";
    private static final String UPDATE =  "UPDATE empleados SET nombreempleado=?, telefono=? WHERE empleadoid=?";
    private static final String DELETE =  "DELETE FROM empleados WHERE empleadoid=?";

    private static final String ORDER_BY = " ORDER BY e.empleadoid ASC";

    private static final String SUM_TOTAL_HORAS_EMPLEADO_BY_ID = "SELECT SUM(p.horas) FROM participan as p " +
                                                                 "INNER JOIN empleados as e " +
                                                                 "ON (p.empleadoid = e.empleadoid) " +
                                                                 "WHERE p.empleadoid =?";

    public int count() {return jdbcTemplate.queryForObject(EmpleadoJDBCDAO.SELECT_COUNT, Integer.class);}

    public List<Empleado> findAll(String search) {

        List<Filtro> filtros = this.generateQueryParams(search);

        List<Empleado> grupos = this.jdbcTemplate.query(
                new PreparedStatementCreator() {
                    @Override
                    public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {

                        String query = SELECT_ALL;

                        for(Filtro filtro: filtros) {
                            query += " WHERE " + filtro.getCondicion() + " ";
                        }

                        query += ORDER_BY;

                        PreparedStatement ps = connection.prepareStatement(query,
                                Statement.CLOSE_CURRENT_RESULT);

                        assignQueryParams(ps, 1, filtros);
                        return ps;
                    }

                },
                new EmpleadoRowMapper()
        );

        return grupos;
    }

    public Empleado findById (Integer id){

        Empleado empleado = this.jdbcTemplate.query(new PreparedStatementCreator() {

            @Override
            public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {

                PreparedStatement ps = connection.prepareStatement(SELECT_BY_ID, Statement.CLOSE_CURRENT_RESULT);
                ps.setInt(1, id);
                return ps;
            }
        }, new EmpleadoResultSetExtractor());
        return empleado;
    }

    public Empleado insert(Empleado empleado) {

        KeyHolder keyHolder = new GeneratedKeyHolder();

        int resultadoOperacion = this.jdbcTemplate.update(
                new PreparedStatementCreator() {
                    @Override
                    public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {

                        PreparedStatement ps =   connection.prepareStatement(INSERT, Statement.CLOSE_CURRENT_RESULT);
                        ps.setString(1, empleado.getNombreempleado());
                        ps.setString(2, empleado.getTelefono());
                        return ps;
                    }
                }, keyHolder);

        if(resultadoOperacion != 1) {
            return null;
        }

        Integer newId;
        //Sistema para coger siempre el id del keyHolder y que no de errores
        if (keyHolder.getKeys().size() > 1) {
            newId = (Integer) keyHolder.getKeys().get("empleadoid");
        } else {
            newId= keyHolder.getKey().intValue();
        }

        empleado.setEmpleadoid(newId);

        return empleado;
    }

    public Boolean update(Empleado empleado) {

        int resultadoOperacion = this.jdbcTemplate.update(
                new PreparedStatementCreator() {
                    @Override
                    public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {

                        PreparedStatement ps =   connection.prepareStatement(UPDATE, Statement.CLOSE_CURRENT_RESULT);
                        ps.setString(1, empleado.getNombreempleado());
                        ps.setString(2, empleado.getTelefono());
                        ps.setInt(3, empleado.getEmpleadoid());
                        return ps;
                    }
                });

        return resultadoOperacion == 1;
    }

    public Boolean delete(Empleado empleado) {

        int resultadoOperacion = this.jdbcTemplate.update(
                new PreparedStatementCreator() {
                    @Override
                    public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {

                        PreparedStatement ps =   connection.prepareStatement(DELETE, Statement.CLOSE_CURRENT_RESULT);
                        ps.setInt(1, empleado.getEmpleadoid());
                        return ps;
                    }
                });

        return resultadoOperacion == 1;
    }
    

    private List<Filtro> generateQueryParams(String searchString) {

        List<Filtro> filtros = new ArrayList<>();
        //TODO funciona pero seguramente tengas que ajustar esta expresion regular para que busque bien
        // por nombreEmpleado
        Pattern pattern = Pattern.compile("([\\w\\d]+)\\.([\\w\\d]+)(:|<|<=|>=|>)([\\w\\d]+)");
        Matcher matcher = pattern.matcher(searchString);

        // Para cada condición
        while (matcher.find()) {

            // Extraemos entidad
            String entidad = matcher.group(1);
            String atributo = matcher.group(2);
            String condicion = matcher.group(3);
            String valor = matcher.group(4);

            Filtro filtro = this.buildCondicion(entidad, atributo, condicion, valor);
            if(filtro != null) {
                filtros.add(filtro);
            }
        }

        return filtros;
    }

    private Filtro buildCondicion(String entidad, String atributo,
                                  String condicion, String valor) {
        
        //TODO Aquí faltaría añadir unas condiciones pero así funciona
        switch (entidad) {

            case "empleado":
                switch (atributo) {

                    case "nombreempleado":
                        if (condicion.equals(":")) {
                            condicion = "like";
                            valor = "%" + valor + "%";
                        }
                        return new Filtro("e.nombreempleado " + condicion + " ? ",
                                TipoFiltro.STRING, valor);

                    case "telefono":
                        if (condicion.equals(":")) {
                            condicion = "like";
                            valor = "%" + valor + "%";
                        }
                        return new Filtro("e.telefono " + condicion + " ? ",
                                TipoFiltro.STRING, valor);

                    default:
                        throw new IllegalArgumentException("Condición no valida.");
                }
                
            case "participan":

                switch (atributo) {
                    case "empleadoid":
                        if (condicion.equals(":")) {
                            condicion = "=";
                        }
                        return new Filtro("p.empleadoid " + condicion + " ? ",
                                TipoFiltro.INTEGER, valor);

                    case "horas":
                        if (condicion.equals(":")) {
                            condicion = "=";
                        }
                        return new Filtro("p.horas " + condicion + " ? ",
                                TipoFiltro.FLOAT, Float.parseFloat(valor));

                    default:
                        throw new IllegalArgumentException("Condición no valida.");
                }

            default:
                throw new IllegalArgumentException("Condición no valida.");
        }
    }

    /**Se encarga de asegurar que el tipo de dato del PreparedStatement sea el correcto */
    private void assignQueryParams(PreparedStatement ps, int initPosition, List<Filtro> filtros) throws SQLException {

        int i = initPosition;
        for(Filtro filtro: filtros) {

            switch (filtro.getTipoFiltro()) {

                case STRING:
                    ps.setString(i, (String)filtro.getValor());
                    break;

                case INTEGER:
                    ps.setInt(i, (Integer)filtro.getValor());
                    break;

                case FLOAT:
                    ps.setFloat(i, (Float)filtro.getValor());
                    break;

                default:
                    System.out.println(filtro.getTipoFiltro());
                    throw new IllegalArgumentException("Condición no valida.");
            }

            i++;
        }
    }

    public Double sumHorasEmpleado(Integer id) {

        Double sumaTotalHorasEmpleado =  jdbcTemplate.query(new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {

                PreparedStatement ps;

                ps = connection.prepareStatement(SUM_TOTAL_HORAS_EMPLEADO_BY_ID, Statement.CLOSE_CURRENT_RESULT);

                ps.setInt(1, id);
                return ps;
            }
        }, new ResultSetExtractor<Double>() {
            @Override
            public Double extractData(ResultSet rs) throws SQLException, DataAccessException {
                //TODO De esta forma podriamos obtener datos de la estrutuctura de la BD como el
                // nombre de las columnas, de una tabla, su tamaño...etc
                //System.out.println(rs.getMetaData().getColumnName(1));
                //System.out.println(rs.getMetaData().getColumnClassName(1));
                //System.out.println(rs.getMetaData().getColumnLabel(1));

                if(!rs.next()){
                    return null;
                }

                return rs.getDouble(1);
            }
        });

        return sumaTotalHorasEmpleado;
    }
}
