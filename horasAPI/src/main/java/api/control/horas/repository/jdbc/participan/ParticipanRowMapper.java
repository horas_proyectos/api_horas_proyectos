package api.control.horas.repository.jdbc.participan;

import api.control.horas.model.Empleado;
import api.control.horas.model.Participan;
import api.control.horas.model.Proyecto;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ParticipanRowMapper implements RowMapper<Participan> {

    Boolean withEmpleado;
    Boolean withProyecto;
    Boolean withAll;

    @Override
    public Participan mapRow(ResultSet rs, int rowNum) throws SQLException {

        Participan participan = new Participan();
        participan.setRegistroid(rs.getInt("registroid"));
        participan.setEmpleadoid(rs.getInt("empleadoid"));
        participan.setProyectoid(rs.getInt("proyectoid"));
        participan.setHoras(rs.getFloat("horas"));

        Empleado empleado = null;
        Proyecto proyecto = null;

        if (withAll) {
            empleado = new Empleado();
            empleado.setEmpleadoid(rs.getInt("empleadoid"));
            empleado.setNombreempleado(rs.getString("nombreempleado"));
            empleado.setTelefono(rs.getString("telefono"));

            proyecto = new Proyecto();
            proyecto.setProyectoid(rs.getInt("proyectoid"));
            proyecto.setNombre(rs.getString("nombre"));
            proyecto.setDescripcion(rs.getString("descripcion"));
        }else if (withEmpleado) {
            empleado = new Empleado();
            empleado.setEmpleadoid(rs.getInt("empleadoid"));
            empleado.setNombreempleado(rs.getString("nombre"));
            empleado.setTelefono(rs.getString("telefono"));
        }else if (withProyecto) {
            proyecto = new Proyecto();
            proyecto.setProyectoid(rs.getInt("proyectoid"));
            proyecto.setNombre(rs.getString("nombre"));
            proyecto.setDescripcion(rs.getString("descripcion"));
        }

        participan.setEmpleado(empleado);
        participan.setProyecto(proyecto);

        return participan;
    }

    public ParticipanRowMapper(Boolean withEmpleado, Boolean withProyecto, Boolean withAll) {
        this.withEmpleado = withEmpleado;
        this.withProyecto = withProyecto;
        this.withAll = withAll;
    }
}
