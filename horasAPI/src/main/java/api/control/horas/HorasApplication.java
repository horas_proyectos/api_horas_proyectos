package api.control.horas;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HorasApplication {

	//TODO Orden de implementación funciones CRUD:
	// 1º count
	// 2º findall normal y luego con el with (son 2 métodos diferentes)
	// 3º findbyid normal y luego con el with
	// 4º create
	// 5º update
	// 6º delete

	//TODO Orden de implementación:
	// 1º Modelo
	// 2º JDBCDAO
	// 3º Controller
	// 4º Y ya las clases que vayas necesitando conforme implementes

	public static void main(String[] args) {
		SpringApplication.run(HorasApplication.class, args);
	}

}
