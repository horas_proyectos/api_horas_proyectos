package api.control.horas.utilities;

public enum TipoFiltro {
    STRING, INTEGER, FLOAT
}
