package api.control.horas.utilities;

/**Clase modelo para construir el filtro*/
public class Filtro {

        public String condicion;
        public TipoFiltro tipoFiltro;
        public Object valor;

        public Filtro(String condicion, TipoFiltro tipoFiltro, Object valor) {
            super();
            this.condicion = condicion;
            this.tipoFiltro = tipoFiltro;
            this.valor = valor;
        }

        public String getCondicion() {
            return condicion;
        }
        public void setCondicion(String condicion) {
            this.condicion = condicion;
        }

        public TipoFiltro getTipoFiltro() {
            return tipoFiltro;
        }
        public void setTipoFiltro(TipoFiltro tipoFiltro) {
            this.tipoFiltro = tipoFiltro;
        }

        public Object getValor() {
            return valor;
        }
        public void setValor(Object valor) {
            this.valor = valor;
        }
    }

