package api.control.horas.controller;

import api.control.horas.model.Empleado;
import api.control.horas.repository.jdbc.empleado.EmpleadoJDBCDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/empleados")
public class EmpleadosController {

    @Autowired
    EmpleadoJDBCDAO empleadoJDBCDAO;

    @GetMapping("/count")
    public Integer count(){
        return this.empleadoJDBCDAO.count();
    }

    @GetMapping("/sum/{id}")
    public Double sum(@PathVariable int id) {
        return this.empleadoJDBCDAO.sumHorasEmpleado(id);
    }

    @GetMapping(value="/")
    public List<Empleado> findAll(@RequestParam(required = false,
                                                 defaultValue = "")
                                                 String search) {
        return this.empleadoJDBCDAO.findAll(search);
    }

    @GetMapping(value="/{id}")
    public Empleado findById(@PathVariable int id) {

        Empleado empleado = null;

        empleado = this.empleadoJDBCDAO.findById(id);

        if(empleado == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,
                    "Entity not found.");
        }
        return empleado;
    }

    @PostMapping("")
    @ResponseStatus(code= HttpStatus.CREATED)
    public Empleado create(@RequestBody Empleado empleado) {

        Empleado empleadoInsertado = null;

        try {
            empleadoInsertado = this.empleadoJDBCDAO.insert(empleado);
        }catch (InvalidDataAccessApiUsageException e){
            e.printStackTrace();
        }
        if(empleadoInsertado == null) {
            throw new ResponseStatusException(HttpStatus.EXPECTATION_FAILED,
                    "Entity not created.");
        }

        return empleadoInsertado;
    }

    @PutMapping("/{id}")
    @ResponseStatus(code=HttpStatus.NO_CONTENT, reason="Entity updated" )
    public void update(@PathVariable int id, @RequestBody Empleado empleado) {

        boolean updated = this.empleadoJDBCDAO.update(empleado);
        if(!updated) {
            throw new ResponseStatusException(HttpStatus.EXPECTATION_FAILED,
                    "Entity not updated.");
        }
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(code=HttpStatus.NO_CONTENT, reason="Entity deleted" )
    public void delete(@PathVariable int id, @RequestBody Empleado empleado) {

        boolean deleted = this.empleadoJDBCDAO.delete(empleado);
        if(!deleted) {
            throw new ResponseStatusException(HttpStatus.EXPECTATION_FAILED,
                    "Entity not deleted.");
        }
    }
}
