package api.control.horas.controller;

import api.control.horas.model.Proyecto;
import api.control.horas.repository.jdbc.proyecto.ProyectoJDBCDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/proyectos")
public class ProyectoController {

    @Autowired
    ProyectoJDBCDAO proyectoJDBCDAO;

    @GetMapping("/count")
    public Integer count(){
        return this.proyectoJDBCDAO.count();
    }

    @GetMapping("/sum/{id}")
    public Double sum(@PathVariable int id) {
        return this.proyectoJDBCDAO.sumHorasProyecto(id);
    }

    @GetMapping(value="/")
    public List<Proyecto> findAll(@RequestParam(required = false,
                                                defaultValue = "")
                                                String search) {
        return this.proyectoJDBCDAO.findAll(search);
    }

    @GetMapping(value="/{id}")
    public Proyecto findById(@PathVariable int id) {

        Proyecto proyecto = null;

        proyecto = this.proyectoJDBCDAO.findById(id);

        if(proyecto == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,
                    "Entity not found.");
        }
        return proyecto;
    }

    @PostMapping("")
    @ResponseStatus(code= HttpStatus.CREATED)
    public Proyecto create(@RequestBody Proyecto proyecto) {

        Proyecto proyectoInsertado = null;

        try {
            proyectoInsertado = this.proyectoJDBCDAO.insert(proyecto);
        }catch (InvalidDataAccessApiUsageException e){
            e.printStackTrace();
        }
        if(proyectoInsertado == null) {
            throw new ResponseStatusException(HttpStatus.EXPECTATION_FAILED,
                    "Entity not created.");
        }

        return proyectoInsertado;
    }

    @PutMapping("/{id}")
    @ResponseStatus(code=HttpStatus.NO_CONTENT, reason="Entity updated" )
    public void update(@PathVariable int id, @RequestBody Proyecto proyecto) {

        boolean updated = this.proyectoJDBCDAO.update(proyecto);
        if(!updated) {
            throw new ResponseStatusException(HttpStatus.EXPECTATION_FAILED,
                    "Entity not updated.");
        }
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(code=HttpStatus.NO_CONTENT, reason="Entity deleted" )
    public void delete(@PathVariable int id, @RequestBody Proyecto proyecto) {

        boolean deleted = this.proyectoJDBCDAO.delete(proyecto);
        if(!deleted) {
            throw new ResponseStatusException(HttpStatus.EXPECTATION_FAILED,
                    "Entity not deleted.");
        }
    }
}
