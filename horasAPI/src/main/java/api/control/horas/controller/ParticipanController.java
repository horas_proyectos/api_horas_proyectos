package api.control.horas.controller;

import api.control.horas.model.Participan;
import api.control.horas.repository.jdbc.participan.ParticipanJDBCDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/participan")
public class ParticipanController {

    @Autowired
    ParticipanJDBCDAO participanJDBCDAO;

    @GetMapping("/count")
    public Integer count() {
        return this.participanJDBCDAO.count();
    }

    @GetMapping("/sum/{idEmpleado}/{idProyecto}")
    public Double sum(@PathVariable int idEmpleado, @PathVariable int idProyecto) {
        return this.participanJDBCDAO.sumHorasParticipan(idEmpleado, idProyecto);
    }

    @GetMapping("/")
    public List<Participan> findAll(@RequestParam(required = false,
                                                defaultValue = "false")
                                                Boolean withEmpleado,
                                    @RequestParam(required = false,
                                                defaultValue = "false")
                                                Boolean withProyecto,
                                    @RequestParam(required = false,
                                                defaultValue = "false")
                                                Boolean withAll){

        return this.participanJDBCDAO.findAll(withEmpleado, withProyecto, withAll);
    }

    @GetMapping("/{id}")
    public Participan findById(@PathVariable int id,
                               @RequestParam(required = false,
                                           defaultValue = "false")
                                           Boolean withEmpleado,
                               @RequestParam(required = false,
                                           defaultValue = "false")
                                           Boolean withProyecto,
                               @RequestParam(required = false,
                                           defaultValue = "false")
                                           Boolean withAll){

        Participan participan = this.participanJDBCDAO.findById(id, withEmpleado, withProyecto, withAll);

        if (participan == null){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Entity not found");
        }
        return participan;
    }

    @PostMapping("")
    @ResponseStatus(code= HttpStatus.CREATED)
    public Participan create(@RequestBody Participan participan) {

        Participan participanInsertado = this.participanJDBCDAO.insert(participan);
        if(participanInsertado == null) {
            throw new ResponseStatusException(HttpStatus.EXPECTATION_FAILED,
                    "Entity not created.");
        }

        return participanInsertado;
    }

    @PutMapping("/{id}")
    @ResponseStatus(code=HttpStatus.NO_CONTENT, reason="Entity updated" )
    public void update(@PathVariable int id, @RequestBody Participan participan) {

        boolean updated = this.participanJDBCDAO.update(participan);
        if(!updated) {
            throw new ResponseStatusException(HttpStatus.EXPECTATION_FAILED,
                    "Entity not updated.");
        }
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(code=HttpStatus.NO_CONTENT, reason="Entity deleted" )
    public void delete(@PathVariable int id, @RequestBody Participan participan) {

        boolean deleted = this.participanJDBCDAO.delete(participan);
        if(!deleted) {
            throw new ResponseStatusException(HttpStatus.EXPECTATION_FAILED,
                    "Entity not deleted.");
        }
    }
}
