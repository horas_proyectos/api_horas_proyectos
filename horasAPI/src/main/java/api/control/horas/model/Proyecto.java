package api.control.horas.model;


import com.fasterxml.jackson.annotation.JsonInclude;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Objects;

@Table(name = "proyectos")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Proyecto {

    @OneToMany
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer proyectoid;
    private String nombre;
    private String descripcion;

    public Integer getProyectoid() {
        return proyectoid;
    }

    public void setProyectoid(Integer proyectoid) {
        this.proyectoid = proyectoid;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Proyecto)) return false;
        Proyecto proyecto = (Proyecto) o;
        return proyectoid.equals(proyecto.proyectoid);
    }

    @Override
    public int hashCode() {
        return Objects.hash(proyectoid);
    }
}
