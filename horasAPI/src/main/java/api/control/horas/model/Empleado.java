package api.control.horas.model;

import com.fasterxml.jackson.annotation.JsonInclude;

import javax.persistence.*;
import java.util.Objects;


@Table(name = "empleados")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Empleado {

    @OneToMany
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer empleadoid;

    private String nombreempleado;
    private String telefono;

    //private List<Participan> participanList = new ArrayList<>();

    public Integer getEmpleadoid() {
        return empleadoid;
    }

    public void setEmpleadoid(Integer empleadoid) {
        this.empleadoid = empleadoid;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getNombreempleado() {
        return nombreempleado;
    }

    public void setNombreempleado(String nombreempleado) {
        this.nombreempleado = nombreempleado;
    }

    public Empleado() {
    }

    public void setEmpleadoid(int empleadoid) {
        this.empleadoid = empleadoid;
    }

//    public List<Participan> getParticipanList() {
//        return participanList;
//    }
//
//    public void setParticipanList(List<Participan> participanList) {
//        this.participanList = participanList;
//    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Empleado)) return false;
        Empleado empleado = (Empleado) o;
        return empleadoid == empleado.empleadoid;
    }

    @Override
    public int hashCode() {
        return Objects.hash(empleadoid);
    }

//    public void addParticipan(Participan participan) {
//        this.participanList.add(participan);
//    }

}
