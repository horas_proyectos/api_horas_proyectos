package api.control.horas.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Table
public class Participan {

    private Integer registroid;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "empleadoid", referencedColumnName = "empleadoid")
    private Integer empleadoid;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "proyectoid", referencedColumnName = "proyectoid")
    private Integer proyectoid;
    //TODO Es posible horas debiera ser una lista de elementos
    private float horas;

    private Empleado empleado;
    private Proyecto proyecto;

    public Participan() {
    }

    public Integer getRegistroid() {
        return registroid;
    }

    public void setRegistroid(Integer registroid) {
        this.registroid = registroid;
    }

    public Integer getEmpleadoid() {
        return empleadoid;
    }

    public void setEmpleadoid(Integer empleadoid) {
        this.empleadoid = empleadoid;
    }

    public Integer getProyectoid() {
        return proyectoid;
    }

    public void setProyectoid(Integer proyectoid) {
        this.proyectoid = proyectoid;
    }

    public float getHoras() {
        return horas;
    }

    public void setHoras(float horas) {
        this.horas = horas;
    }

    public Empleado getEmpleado() {
        return empleado;
    }

    public Proyecto getProyecto() {
        return proyecto;
    }

    public void setProyecto(Proyecto proyecto) {
        this.proyecto = proyecto;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Participan)) return false;
        Participan that = (Participan) o;
        return registroid.equals(that.registroid);
    }

    @Override
    public int hashCode() {
        return Objects.hash(registroid);
    }

    public void setEmpleado(Empleado empleado) {
        this.empleado = empleado;
    }
}
